﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPrefsManager : MonoBehaviour {

    const string MASTERVOLUMEKEY = "mastervolume";
    const string LEVELKEY = "level_unlocked_";
    const string HIGHSCORE = "";


    public static void SetHighScore(float score,float levelindex)
    {
        if(score > PlayerPrefs.GetFloat(HIGHSCORE))
        {
            //PlayerPrefs.SetFloat(HIGHSCORE, score);
            PlayerPrefs.SetFloat(HIGHSCORE + levelindex.ToString(), score);
        }
        else
        {
            //don't allow setting high score
            //PlayerPrefs.SetFloat(HIGHSCORE+levelindex.ToString(), score); // this is here only for development it has to be deleted when publishing
        }
    }


    public static float GetHighScore(int level)
    {
        float a = PlayerPrefs.GetFloat(HIGHSCORE + level.ToString());
        Debug.Log(PlayerPrefs.GetFloat(HIGHSCORE + level.ToString()));
        return (PlayerPrefs.GetFloat(HIGHSCORE+level.ToString()));
    }




	public static void SetMasterVolume(float volume)
    {
        if(volume >= 0f && volume <= 1f)
        {
            PlayerPrefs.SetFloat(MASTERVOLUMEKEY, volume);
        }
        else
        {
            Debug.LogError("SetMasterVolume in PlayerPrefsManager not working");
        }
    }

    public static float GetMasterVolume()
    {
        return PlayerPrefs.GetFloat(MASTERVOLUMEKEY);
    }



    public static bool IsLevelUnlocked(int level)
    {
        int levelValue = PlayerPrefs.GetInt(LEVELKEY + level.ToString());
        bool isLevelUnlocked = (level == 1);
        if(level <= SceneManager.sceneCountInBuildSettings - 1)
        {
            return isLevelUnlocked;
        }
        else
        {
            Debug.LogError("Nemoguće je otključati nepostojeći level");
            return false;
        }
    }


    public static void UnlockLevel(int level)
    {
        if (level <= SceneManager.sceneCountInBuildSettings - 1)
        {
            PlayerPrefs.SetInt(LEVELKEY+level.ToString(), 1);
        }
        else
        {
            Debug.LogError("Nemoguće je otključati nepostojeći level");            
        }
    }
}
