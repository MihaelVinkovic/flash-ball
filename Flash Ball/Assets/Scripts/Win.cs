﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Win : MonoBehaviour {

    public GameObject mainCanvas;
    public GameObject winCanvas;
    public PlayerController playerController;
    public GameObject player;
    public bool levelpassed;
	// Use this for initialization
	void Start () {
        winCanvas.SetActive(false);
        levelpassed = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            levelpassed = true;
            Time.timeScale = 0;
            player.SetActive(false);
            playerController.SetMoving(false);
            mainCanvas.SetActive(false);
            winCanvas.SetActive(true);
            playerController.gameOver = false;
            
            
        }
    }
}
