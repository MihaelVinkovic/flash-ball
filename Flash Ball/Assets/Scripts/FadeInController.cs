﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInController : MonoBehaviour {

    private float fadeInTime = 2f;
    private Image panel;
    private Color currentColor = Color.black;
    private MusicManager musicManager;

	// Use this for initialization
	void Start () {
        panel = GetComponent<Image>();
        gameObject.SetActive(true);
        musicManager = GameObject.FindObjectOfType<MusicManager>();        
	}
	
	// Update is called once per frame
	void Update () {
        if (musicManager.sviraj == true)
        {

            if (Time.timeSinceLevelLoad < fadeInTime)
            {
                float alphaChange = Time.deltaTime / fadeInTime;
                currentColor.a -= alphaChange;
                panel.color = currentColor;
            }
            else
            {
                gameObject.SetActive(false);

            }
        }
        else
        {
            gameObject.SetActive(false);
        }
	}    
}
