﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    private GameObject Player;
    private Vector3 offset;
    // Use this for initialization
    void Start()
    {
        Player = GameObject.Find("Player");
        offset = transform.position - Player.transform.position;
        //print(Player);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, 0) + offset;

    }



    //public Transform target;
    //public float smoothSpeed = 0.125f;
    //public Vector3 offset;


    //private void LateUpdate()
    //{
    //    Vector3 desiredPosition = target.position + offset;
    //    Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
    //    transform.position = smoothPosition;
    //}
}
