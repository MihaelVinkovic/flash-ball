﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpController : MonoBehaviour {

    public float rotationSpeed = 3;    
    private ScoreController score;
	// Use this for initialization
	void Start () {
        score = GameObject.FindObjectOfType<ScoreController>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.eulerAngles += new Vector3(0, 0, rotationSpeed);
	}


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {            
            score.UpdateScore(1f);
            gameObject.SetActive(false);
        }
    }
}
