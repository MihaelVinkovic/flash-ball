﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

// u Score controleru bi mogli koristiti scene manager 
/*u start funkciji bi dobili index scene (levela) 
 koja bi isla po listi i spremala u listu na mjesto=level.buildindex 
 sprema*/


public class ScoreController : MonoBehaviour {

    public TextMeshProUGUI myText;
    public Win win;


    private float score =0f;
    private float[] scorePerLevel = new float[10];
    private int LevelIndex;

	// Use this for initialization
	void Start ()
    {
        LevelIndex = SceneManager.GetActiveScene().buildIndex;
        myText.text = "SCORE : " + score.ToString();
        //PlayerPrefsManager.SetHighScore(0,LevelIndex); // this is here only for development        
        print("Level index: "+LevelIndex);
        for (int i = 0; i < scorePerLevel.Length; i++)
            scorePerLevel[i] = 0;
    }
	
	// Update is called once per frame
	void Update ()
    {
        myText.text = "SCORE : " + score.ToString();
        if (win.levelpassed) // ako je level prijeđeni postavi score u array
        {
            if (scorePerLevel[LevelIndex] < score)// ako je trenutni score veci od scora koji je zapisan u arrayu onda ga postavi kao novi high score
            {
                scorePerLevel[LevelIndex] = score;
                PlayerPrefsManager.SetHighScore(score,LevelIndex);                
                Debug.Log(PlayerPrefsManager.GetHighScore(LevelIndex));
                string a = PlayerPrefsManager.GetHighScore(LevelIndex).ToString();
                PlayerPrefsManager.GetHighScore(LevelIndex);
            }
        }
    }

    public void UpdateScore(float amount)
    {
        score += amount;
       
        if (win.levelpassed) // ako je level prijeđeni postavi score u array
        {
            if (scorePerLevel[LevelIndex] < score)// ako je trenutni score veci od scora koji je zapisan u arrayu onda ga postavi kao novi high score
            {
                scorePerLevel[LevelIndex] = score;
                PlayerPrefsManager.SetHighScore(score,LevelIndex);
                Debug.Log(PlayerPrefsManager.GetHighScore(LevelIndex));
            }
        }
    }
}
