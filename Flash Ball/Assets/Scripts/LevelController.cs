﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour {

    public LevelManager levelManager;
    public Image[] slika;
    public Text text;
    public GameObject panel;
    //public Button gumb;
    private Color boja = Color.white;
    private int y = 4; // level 1 pocinje na sceni s indexom 4, level 2->5, level 3->6,level 4->7,level 5->8 zato pocinjemo od 4
    private int zbrojZvijezdica = 0;
    int brojac = 5;
	// Use this for initialization
	void Start () {
        boja.a = 255;  // 255 je maksimalno bez alphe znači čista boja
        for (int i = 1; i < brojac; i++)
        {
            //for(int y = 4; y < 6; y++)
            //{
            if (PlayerPrefsManager.GetHighScore(y) == 1)
            {
                slika[i].fillAmount = 0.369f;
                zbrojZvijezdica += 1;
            }else if(PlayerPrefsManager.GetHighScore(y) == 2)
            {
                slika[i].fillAmount = 0.668f;
                zbrojZvijezdica += 2;
            }else if (PlayerPrefsManager.GetHighScore(y) == 3)
            {
                slika[i].fillAmount = 1f;
                zbrojZvijezdica += 3;
            }
            y++;
            if(zbrojZvijezdica >= 12)
            {
                brojac = 6;
                text.color = boja;
            }
            //}
        }
	}

    void Zbroj()
    {
        for (int i = 1; i < brojac; i++)
        {
            //for(int y = 4; y < 6; y++)
            //{
            if (PlayerPrefsManager.GetHighScore(y) == 1)
            {
                //slika[i].fillAmount = 0.369f;
                zbrojZvijezdica += 1;
            }
            else if (PlayerPrefsManager.GetHighScore(y) == 2)
            {
                //slika[i].fillAmount = 0.668f;
                zbrojZvijezdica += 2;
            }
            else if (PlayerPrefsManager.GetHighScore(y) == 3)
            {
                //slika[i].fillAmount = 1f;
                zbrojZvijezdica += 3;
            }
            y++;
            if (zbrojZvijezdica >= 12)
            {
                brojac = 6;
                //text.color = boja;
            }
            //}
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void GoBack()
    {
        levelManager.LoadLevel("MainMenu");
    }

    public void Unlocked()
    {
        if(zbrojZvijezdica >= 12)
        {
            print("broj:" + zbrojZvijezdica);
            levelManager.LoadLevel("Level5");
        }else if(zbrojZvijezdica < 12)
        {
            panel.SetActive(true);
            Invoke("PanelDisable", 1.5f);
        }
    }

    void PanelDisable()
    {
        panel.SetActive(false);
    }

    public void CanvasToLevel5()
    {
        Zbroj();
        if (zbrojZvijezdica >= 12)
        {
            print(zbrojZvijezdica);
            levelManager.LoadLevel("Level5");
        }
        else
        {
            print("ZBROJ: "+zbrojZvijezdica);
            levelManager.LoadLevel("Levels");
        }
    }

}
