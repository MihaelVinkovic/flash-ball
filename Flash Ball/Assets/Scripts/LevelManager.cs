﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {


    public float timeToLoadNextLevel = 0f;    

    private void Start()
    {
        if(timeToLoadNextLevel <= 0f)
        {
           // automatic load level disabled
        }
        else
        {
            Invoke("LoadNextLevel", timeToLoadNextLevel);
        }
    }

    public void LoadLevel(string name)
    {
        Time.timeScale = 1;  // ovo je ovdje jer se mora vratiti timeScale na 1 prilikom pritiksa na play again u canvasu kad pobijedimo ili izgubimo
        SceneManager.LoadScene(name);
    }



    public void LoadNextLevel()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
   

   
    public void Exit()
    {
        Application.Quit();
    }
	
}
