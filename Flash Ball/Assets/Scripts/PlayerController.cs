﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    private Rigidbody2D thisRigidBody;
        // Ovo je za provjeru ako je Player prizemljen 
    private bool Grounded = false;


        // Ovo je za provjeru ako je igrač završio igru
    public bool gameOver = true;
    public GameObject LoseCanvas;
    public GameObject mainCanvas;
    public float speedMultiplier = 2f;

	// Use this for initialization
	void Start () {
        thisRigidBody = GetComponent<Rigidbody2D>();
        LoseCanvas.SetActive(false);       
	}
	
	// Update is called once per frame
	void Update () {        
        if (gameOver)
        {
            //transform.position += new Vector3(speedMultiplier, 0, 0);      // kretanje igrača
            

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) // ako je pritisnuta tipka space provjeri ako je player prizemljen i ako je skoči ako nije nemoj skočiti, akoje korisnik prtisnuo ekran
            {
                if (Grounded)
                {
                    thisRigidBody.AddForce(new Vector3(0, 180f,0));
                    Grounded = false;
                }

            }
        }
        
	}

    private void FixedUpdate()
    {
        transform.position += Vector3.right * speedMultiplier * Time.deltaTime;
    }

    public void SetMoving(bool moving)
    {
        gameOver = moving;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "NoGood" || collision.gameObject.name == "Shreder")
        {
            Debug.Log("err");
            Debug.Log("pimpek1: " + collision.gameObject.name);
            SetMoving(false);
            mainCanvas.SetActive(false);
            LoseCanvas.SetActive(true);
            gameObject.SetActive(false);
            Time.timeScale = 0;

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
            // Ako je Player udario u Pod postavi Grounded u true
        if(collision.gameObject.tag == "Pod")
        {           
            Grounded = true;
        }


            // Ako je Player udario u gameObject koji ima tag "NoGood"
        if(collision.gameObject.tag == "NoGood" || collision.gameObject.name=="Shreder")
        {
            Debug.Log("pimpek: "+collision.gameObject.name);
            SetMoving(false);
            mainCanvas.SetActive(false);
            LoseCanvas.SetActive(true);
            gameObject.SetActive(false);
            Time.timeScale = 0;

        }
    }


   
}
