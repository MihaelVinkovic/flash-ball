﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {

    public AudioClip[] zvukovi;
    private AudioSource audioSource;
    private LevelManager levelManager;

    public bool sviraj = true;

    public void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start () {        
        audioSource = GetComponent<AudioSource>();
        levelManager = GameObject.FindObjectOfType<LevelManager>();        
        audioSource.volume = PlayerPrefsManager.GetMasterVolume();
        
        if(SceneManager.GetActiveScene().buildIndex == 0)
        {
            PlayerPrefsManager.SetMasterVolume(0.5f);
        }
	}

    private void OnLevelWasLoaded(int level)
    {
        /* ako je korisnik prilikom pokretanaj igrice došau u main menu i otišao u options te se 
        vrača u main menu a u listi audio clipova ima clip za main menu tako da mu se u toj situaciji kad 
        prolazi main menu systemom ne pokreće sound svaki put ispocetka kad otide u options i vrati se  ili 
        levels i vrati se nego se clip neprekidno reproducira*/

        if(level == 2 || level == 3)
        {
            sviraj = false;          
        }else if(level >= 4)
        {
            sviraj = true;         
        }
        
        AudioClip thisLevelMusic = zvukovi[level];
        if (sviraj)
        {
            if (thisLevelMusic) // ako ima prikvacene pjesme
            {
                audioSource.clip = thisLevelMusic;
                audioSource.loop = true;
                audioSource.Play();

            }
        }
    }

    public void ChangeVolume(float volume)
    {
        audioSource.volume = volume;
    }
}
