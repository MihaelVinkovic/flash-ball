﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour {

    public Slider slider;
    public LevelManager levelManager;

    private MusicManager musicManager;


	// Use this for initialization
	void Start () {
        musicManager = GameObject.FindObjectOfType<MusicManager>();
        slider.value = PlayerPrefsManager.GetMasterVolume();
	}
	
	// Update is called once per frame
	void Update () {
        musicManager.ChangeVolume(slider.value);
	}

    public void SaveAndExit()
    {
        PlayerPrefsManager.SetMasterVolume(slider.value);
        levelManager.LoadLevel("MainMenu");
    }


    public void SetDefaults()
    {
        slider.value = 0.3f;
    }
}
