﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PauseController : MonoBehaviour {


    public GameObject mainCanvas;
    public GameObject pauseCanvas;

    public PlayerController playerController;
    public GameObject player;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void Paused()
    {
        
        player.SetActive(false);
        playerController.SetMoving(false);
        mainCanvas.SetActive(false);
        pauseCanvas.SetActive(true);
        playerController.gameOver = false;
        Time.timeScale = 0;

    }



    public void Continue()
    {
        player.SetActive(true);
        playerController.SetMoving(true);
        mainCanvas.SetActive(true);
        pauseCanvas.SetActive(false);
        playerController.gameOver = true;
        Time.timeScale = 1;
    }
}
